appeased_spirits = {
	icon = 8
	local_revolt_risk = -0.25
}
angry_spirits = {
	icon = 26
	local_revolt_risk = 0.25
}
avatargroupie = {
	icon = 18
	general_opinion = 5
}
exbender = {
	icon = 36
	monthly_character_prestige = -3.0
}

guru = {
	icon = 2
	monthly_character_piety = 3.0
	learning = 6
}
beifong = {
	icon = 9
	monthly_character_wealth = 0.1
	stewardship = 1
}
yuyan = {
	icon = 1
	martial = 1
	intrigue = 2
}

ingenious = {
	icon = 5
	diplomacy = 1
	martial = 1
	intrigue = 2
	stewardship = 1
	learning = 2
}

foggyswamptribe = {
	icon = 1
	diplomacy = -1
	martial = 3
	learning = -3
	monthly_character_piety = 2.0
}

sunwarrior = {
	icon = 13
	diplomacy = -2
	martial = 1
	monthly_character_piety = 0.75
}

driven = {
	icon = 18
	martial = 5
	learning = -2
	diplomacy = -1
	monthly_character_piety = -0.5
	church_opnion = -15
}

studying_foreign_bending = {
	icon = 2
	martial = 2
	learning = 3
}
training = {
	icon = 1
	martial = 1
}
local_factories = {
	local_build_time_modifier = -0.15
	local_build_cost_modifier = -0.2
	local_tax_modifier = 0.1
	levy_size = 0.1
	supply_limit = 2.5
	disease_defence = 0.1
	icon = 12
}
temple_outpost = {
	local_build_time_modifier = 2
	local_build_cost_modifier = 2
	local_revolt_risk = -0.2
	local_tax_modifier = -0.95
	levy_size = -0.95
	supply_limit = -2.5
	major = yes
	icon = 7
}
survived_genocide = {
	martial = 1
	icon = 13
}
power_of_the_comet = {
	siege_speed = 0.5
	land_morale = 0.25
	land_organisation = 0.25
	icon = 41
}
navy_shipyards = {
	galleys_perc = 4
	icon = 1
}
south_raiders_base = {
	levy_size = 0.5
	galleys_perc = 20
	supply_limit = 5.0
	icon = 13
}
building_colony = {
	local_build_time_modifier = 0.2
	local_build_cost_modifier = 0.2
	local_tax_modifier = -0.2
	levy_size = -0.2
	local_revolt_risk = 0.05
	icon = 27
}
restored_ba_sing_sa = {
	levy_size = 0.02
	local_tax_modifier = 0.02
	siege_defence = 0.1
	local_revolt_risk = -0.02
	icon = 1
}
collapsed_wall = {
	siege_defence = -0.5
	icon = 1
}
lowered_taxes = {
	local_build_time_modifier = 0.05
	local_build_cost_modifier = 0.05
	local_tax_modifier = -0.05
	local_revolt_risk = -0.1
	icon = 24
}
greater_omashu = {
	levy_size = 0.05
	local_tax_modifier = 0.1
	siege_defence = 0.1
	local_revolt_risk = -0.05
	icon = 1
}
wolves_threaten_tribe = {
	local_tax_modifier = -0.5
	icon = 25
}
firebender_unrest = {
	levy_size = -0.2
	local_revolt_risk = 0.05
	icon = 31
}
comprimised_operations = {
	intrigue = -2
	icon = 23
}
convert_operations = {
	intrigue = 2
	icon = 5
}
damaged_factory = {
	local_tax_modifier = -0.15
	local_revolt_risk = 0.05
	disease_defence = -0.15
	icon = 30
}

colony_1 = {
	local_build_time_modifier = 0.5
	local_build_cost_modifier = 0.5
	local_tax_modifier = -0.5
	levy_size = -0.5
	local_revolt_risk = 0.1
	icon = 8
	major = yes
}

colony_2 = {
	local_build_time_modifier = 0.25
	local_build_cost_modifier = 0.25
	local_tax_modifier = -0.25
	levy_size = -0.25
	local_revolt_risk = 0.05
	icon = 9
	major = yes
}

colony_3 = {
	local_build_time_modifier = 0.1
	local_build_cost_modifier = 0.1
	local_tax_modifier = -0.1
	levy_size = -0.1
	local_revolt_risk = 0.05
	icon = 10
	major = yes
}

dai_li_outpost = {
	icon = 23
}

dai_li_stronghold = {
	icon = 23
}

dai_li_headquarters = {
	icon = 32
}

supporting_the_earth_kingdom = {
	icon = 24
	global_tax_modifier = -0.4
	city_vassal_tax_modifier = -0.1
	castle_vassal_tax_modifier = -0.1
	temple_vassal_tax_modifier = -0.1
}

supporting_the_earth_kingdom_overextended = {
	icon = 24
	global_tax_modifier = -0.7
	city_vassal_tax_modifier = -0.5
	castle_vassal_tax_modifier = -0.5
	temple_vassal_tax_modifier = -0.5
}

conquerer_5_years = {
	levy_size = 0.1
	icon = 13
}

chin_fall_modifier = {
	local_tax_modifier = -0.15
	local_revolt_risk = 0.1
	disease_defence = -0.15
	levy_size = -0.2
	garrison_size = -0.2
	icon = 31
}

condemned_by_avatar = {
	icon = 21
	diplomacy = -1
}

royal_army_decay_overgrowth = {
	icon = 19
	levy_reinforce_rate = -0.25
	levy_size = -0.1
	land_morale = -0.1
	land_organisation = -0.1
	siege_speed = -0.05
}

royal_army_decay_collapse = {
	icon = 19
	levy_reinforce_rate = -0.2
	land_morale = -0.1
	land_organisation = -0.1
	levy_size = -0.05
	siege_defence = -0.05
}

agni_blessing = {
	health = 2
	icon = 50
}

ek_tyranny = {
	global_revolt_risk = 0.05
	diplomacy_penalty = 1
	general_opinion = -10
	icon = 25
}

ek_good_authority = {
	diplomacy = 1
	general_opinion = 10
	icon = 7
}
age_of_uncertainty_malus = {
	icon = 96
	disease_defence = -0.05
}

age_of_rebellion_malus = {
	icon = 96
	local_revolt_risk = 0.1
	local_tax_modifier = -0.05
}

age_of_rebellion_malus_world = {
	icon = 96
	local_revolt_risk = 0.01
	local_tax_modifier = -0.05
}

age_of_prosperity_malus = {
	icon = 96
	local_tax_modifier = 0.1
}

age_of_aggression_malus = {
	icon = 96
	local_tax_modifier = -0.05
	levy_size = 0.1
}

reunificationEffortsBoon = {
	icon = 1
	levy_reinforce_rate = 0.4
	levy_size = 0.4
}

marketHosted = {
	icon = 4
	local_tax_modifier = 0.01
}

marketBoughtArms = {
	icon = 13
	levy_reinforce_rate = 0.3
	levy_size = 0.2
}

marketBoughtMeds = {
	icon = 54
	disease_defence = 0.05
}

marketBoughtCons = {
	icon = 4
	build_time_modifier = -0.2
	build_cost_modifier = -0.1
}

marketBoughtGoods = {
	icon = 4
	local_tax_modifier = 0.02
	trade_route_value = 1
	trade_route_wealth = 5
}

cp_boost_trade = {
	icon = 11
	disease_defence = -0.05
	build_time_modifier = -0.05
	build_cost_modifier = -0.05
	local_tax_modifier = 0.05
}

cp_boost_protection = {
	icon = 29
	disease_defence = 0.05
}

white_lotus_safeguard_effect = {
	icon = 5
	defensive_plot_power_modifier = 0.5
}

can_summon_plague_sprit = {
	is_visible = { character = FROM }
	icon = 74
}

white_lotus_stir = {
	icon = 34
	local_revolt_risk = 0.1
}

province_malus_comet_damage = {
	icon = 32
	build_time_modifier = 0.1
	build_cost_modifier = 0.1
	local_tax_modifier = -0.25
}

ba_sing_se_besieged = {
	icon = 32
	local_tax_modifier = -0.25
	levy_reinforce_rate = -0.3
	levy_size = -0.2
}

buff_legendary_hero = {
	icon = 7
	general_opinion = 40
}