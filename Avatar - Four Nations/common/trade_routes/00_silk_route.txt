taki_trade_route = {
	wealth = 24
	
	modifier = {
		castle_tax_modifier = 0.1
		city_tax_modifier = 0.1
		temple_tax_modifier = 0.1
		trade_route_value = 1
		local_movement_speed = 0.20
		icon = 4
	}

	start = {
		79 # Taki
	}
	
	trade_post_display_trigger = {
		OR = {
			province_id = 7
			province_id = 61
			province_id = 72
			province_id = 79
			province_id = 84
			province_id = 95
			province_id = 228
			province_id = 239
			province_id = 246
			province_id = 340
			province_id = 568
		}
	}
	
	path = {	# Taki > Somonyama
		79 80 84
	}
	path = {	# Somonyama > Yu Dao  
		84 83 87 90 92 98 99 100 101 102 103 104 105 235 228
	}
	path = {	# Somonyama > Gaoling
		84 85 94 95 568 356 354 353 340
	}
	path = {	# Taki > Muli
		79 81 82 61 64 66 72
	}
	path = {	# Muli > Northern Water Tribe
		72 225 19 559 18 17 15 14 7
	}
	path = {	# Muli > Yu Dao
		72 247 246 245 239 238 231 229 228
	}
	
}

yu_dao_trade_route = {
	wealth = 16
	
	modifier = {
		castle_tax_modifier = 0.1
		city_tax_modifier = 0.1
		temple_tax_modifier = 0.1
		tribal_tax_modifier = 0.1
		trade_route_value = 1
		local_movement_speed = 0.20
		icon = 4
	}
	
	start = {
		228 # Yu Dao
	}
	
	trade_post_display_trigger = {
		OR = {
			province_id = 7
			province_id = 207
			province_id = 214
			province_id = 228
			province_id = 234
			province_id = 273
			province_id = 309
			province_id = 315
			province_id = 340
		}
	}
	
	path = {	# Yu Dao > Badaji
		228 576 232 216 589 214
	}
	path = {	# Yu Dao > Yantai
		228 236 234
	}
	path = {	# Badaji > Gaoling
		214 253 255 257 273 274 314 309 310 316 315 308 322 320 319 352 348 340
	}
	path = {	# Badaji > Northern Water Tribe
		214 215 210 206 207 208 540 220 16 7
	}
}

omashu_trade_route = {
	wealth = 4
	
	modifier = {
		castle_tax_modifier = 0.05
		city_tax_modifier = 0.05
		temple_tax_modifier = 0.05
		tribal_tax_modifier = 0.1
		trade_route_value = 1
		local_movement_speed = 0.20
		icon = 4
	}
	
	trade_post_display_trigger = {
		OR = {
			province_id = 283
			province_id = 281
			province_id = 309
		}
	}
	
	start = {
		309 # Omashu
	}
	
	path = {	# Omashu > Nanwazi
		309 581 312 298 299 586 290 281 279 285 283
	}
}

gaoling_trade_route = {
	wealth = 16
	
	modifier = {
		castle_tax_modifier = 0.1
		city_tax_modifier = 0.1
		temple_tax_modifier = 0.1
		tribal_tax_modifier = 0.1
		trade_route_value = 1
		local_movement_speed = 0.20
		icon = 4
	}
	
	start = {
		340 # Gaoling
	}
	
	trade_post_display_trigger = {
		OR = {
			province_id = 20
			province_id = 25
			province_id = 37
			province_id = 166
			province_id = 168
			province_id = 289
			province_id = 340
			province_id = 366
			province_id = 386
			province_id = 376
			province_id = 568
			province_id = 569
		}
	}
	
	path = {	# Gaoling > Tongdoa
		340 341 343 370 369 368 372 371 376
	}
	path = {	# Gaoling > Southern Water Tribe
		340 342 367 366 386 387 572 571 570 569 37 25 24 22 20
	}
	path = {	# Tongoda > Jinzhong
		376 379 166 167 161 162 160 159 155 152 151 150 168
	}
	path = {	# Tongoda > Sunan
		376 374 375 289
	}
}