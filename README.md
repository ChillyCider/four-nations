# README #
Hello, and welcome to the Avatar: Four Nations mod repository. In order to drive future development, this repository has been opened to all to contribute to the mod. 

### Contribution guidelines ###
While there are few solid guidelines, but please be sure to keep a high standard for all submissions, and always maintain a collaborative environment. Creating new content can be fun, but its always important to keep in mind keeping the mod as stable and relatively bug free as possible.

### Who do I talk to? ###
For any inquiries, the fastest way to get a response to post on the mod's Paradox Forum page, located at: https://forum.paradoxplaza.com/forum/index.php?threads/the-four-nations-an-avatar-the-last-airbender-mod-release.878195/